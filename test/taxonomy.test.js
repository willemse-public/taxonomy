const taxonomy = require('../taxonomy');

test('Null and empty', () => {
  expect(taxonomy()).toBeNull();
  expect(taxonomy(null)).toBeNull();
  expect(taxonomy('')).toBeNull();
  expect(taxonomy(' ')).toBeNull();
});

test('Genus, hybrids and chimeara', () => {
  expect(taxonomy('Brassica').genus).toBe('Brassica');
  expect(taxonomy('Brassica').isHybridGenus).toBeFalsy();
  expect(taxonomy('Brassica').isChimaera).toBeFalsy();
  expect(taxonomy('x Brassica').genus).toBe('x Brassica');
  expect(taxonomy('x Brassica').isHybridGenus).toBeTruthy();
  expect(taxonomy('x Brassica').isChimaera).toBeFalsy();
  expect(taxonomy('xBrassica').genus).toBe('x Brassica');
  expect(taxonomy('xBrassica').isHybridGenus).toBeTruthy();
  expect(taxonomy('xBrassica').isChimaera).toBeFalsy();
  expect(taxonomy('×Brassica').genus).toBe('x Brassica');
  expect(taxonomy('×Brassica').isHybridGenus).toBeTruthy();
  expect(taxonomy('×Brassica').isChimaera).toBeFalsy();
  expect(taxonomy('+ Brassica').genus).toBe('+ Brassica');
  expect(taxonomy('+ Brassica').isHybridGenus).toBeFalsy();
  expect(taxonomy('+ Brassica').isChimaera).toBeTruthy();
  expect(taxonomy('+Brassica').genus).toBe('+ Brassica');
  expect(taxonomy('+Brassica').isHybridGenus).toBeFalsy();
  expect(taxonomy('+Brassica').isChimaera).toBeTruthy();
  expect(taxonomy('Brassica oleracea').genus).toBe('Brassica');
  expect(taxonomy("Forsythia 'Courtasol' MARÉE D'OR®").genus).toBe('Forsythia');
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. serrata (Thunb.) Makino.').genus).toBe('Hydrangea');
  expect(taxonomy('BRASSICA').genus).toBe('Brassica');
  expect(taxonomy('brassica').genus).toBe('Brassica');
  expect(taxonomy('+brassica').genus).toBe('+ Brassica');
  expect(taxonomy('×BRASSICA').genus).toBe('x Brassica');
  expect(taxonomy('BRASSICA OLERACEA').genus).toBe('Brassica');
});

test('Species', () => {
  expect(taxonomy('Brassica oleracea').species).toBe('oleracea');
  expect(taxonomy('Brassica ×oleracea').species).toBe('x oleracea');
  expect(taxonomy('Brassica ×oleracea').isHybridSpecies).toBeTruthy();
  expect(taxonomy('Brassica ×oleracea').isHybridGenus).toBeFalsy();
  expect(taxonomy('Brassica × oleracea').species).toBe('x oleracea');
  expect(taxonomy('Brassica × oleracea').isHybridSpecies).toBeTruthy();
  expect(taxonomy('Brassica × oleracea').isHybridGenus).toBeFalsy();
  expect(taxonomy('Brassica x oleracea').species).toBe('x oleracea');
  expect(taxonomy('Brassica x oleracea').isHybridSpecies).toBeTruthy();
  expect(taxonomy('Brassica x oleracea').isHybridGenus).toBeFalsy();
  expect(taxonomy("Rosa 'Tanky' WHISKY").species).toBeNull();
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. macrophylla').species).toBe('macrophylla');
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. serrata (Thunb.) Makino.').species).toBe('macrophylla');
  expect(taxonomy('Phegopteris connectilis (Michx.) Watt').species).toBe('connectilis');
  expect(taxonomy('BRASSICA OLERACEA').species).toBe('oleracea');
  expect(taxonomy('BRASSICA ×OLERACEA').species).toBe('x oleracea');
  expect(taxonomy('HYDRANGEA MACROPHYLLA (THUNB.) SPRENG. SUBSP. MACROPHYLLA').species).toBe('macrophylla');
});

test('Subspecies', () => {
  expect(taxonomy('Hydrangea macrophylla subsp. serrata').genus).toBe('Hydrangea');
  expect(taxonomy('Hydrangea macrophylla subsp. serrata').species).toBe('macrophylla');
  expect(taxonomy('Hydrangea macrophylla subsp. serrata').subspecies).toBe('serrata');
  expect(taxonomy('Hydrangea macrophylla subsp serrata').subspecies).toBe('serrata');
  expect(taxonomy('Hydrangea macrophylla subsp serrata').variety).toBeNull();
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. macrophylla').subspecies).toBe('macrophylla');
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. serrata (Thunb.) Makino.').subspecies).toBe('serrata');
  expect(taxonomy('HYDRANGEA MACROPHYLLA SUBSP. SERRATA').genus).toBe('Hydrangea');
  expect(taxonomy('HYDRANGEA MACROPHYLLA SUBSP. SERRATA').species).toBe('macrophylla');
  expect(taxonomy('HYDRANGEA MACROPHYLLA SUBSP. SERRATA').subspecies).toBe('serrata');
  expect(taxonomy('HYDRANGEA MACROPHYLLA SUBSP SERRATA').subspecies).toBe('serrata');
  expect(taxonomy('HYDRANGEA MACROPHYLLA SUBSP SERRATA').variety).toBeNull();
});

test('Variety', () => {
  expect(taxonomy('Hydrangea macrophylla var. hattoriana').genus).toBe('Hydrangea');
  expect(taxonomy('Hydrangea macrophylla var. hattoriana').species).toBe('macrophylla');
  expect(taxonomy('Hydrangea macrophylla var. hattoriana').subspecies).toBeNull();
  expect(taxonomy('Hydrangea macrophylla var. hattoriana').variety).toBe('hattoriana');
  expect(taxonomy('Hydrangea macrophylla var hattoriana').variety).toBe('hattoriana');

  expect(taxonomy('HYDRANGEA MACROPHYLLA VAR. HATTORIANA').variety).toBe('hattoriana');
  expect(taxonomy('HYDRANGEA MACROPHYLLA VAR HATTORIANA').variety).toBe('hattoriana');
});

test('Cultivar', () => {
  expect(taxonomy('Hydrangea macrophylla var. hattoriana').cultivar).toBeNull();
  expect(taxonomy("Apium graveolens var. rapaceum 'Giant Prague'").cultivar).toBe('Giant Prague');
  expect(taxonomy('Lilium BLANCHE NEIGE (‘Belosnezhka’)').cultivar).toBe('Belosnezhka');
  expect(taxonomy("Lilium BLANCHE NEIGE ('Belosnezhka')").cultivar).toBe('Belosnezhka');
  expect(taxonomy("Begonia ( Pendula Group ) 'Cascade Florence' ( Cascade series )").cultivar).toBe('Cascade Florence');
  expect(taxonomy("Forsythia 'Courtasol' MARÉE D'OR®").cultivar).toBe('Courtasol');
  expect(taxonomy("Erica x darleyensis 'Lucie' ( WINTER BELLES® series )").cultivar).toBe('Lucie');
  expect(taxonomy("Phlox divaricata subsp. laphamii 'Chattahoochee'").cultivar).toBe('Chattahoochee');
  expect(taxonomy("Allium porrum 'Gros long d'été 2'").cultivar).toBe("Gros Long d'Été 2");
  expect(taxonomy("Fuchsia 'Delta's Sarah' BLUE SARAH").cultivar).toBe("Delta's Sarah");

  expect(taxonomy('HYDRANGEA MACROPHYLLA VAR. HATTORIANA').cultivar).toBeNull();
  expect(taxonomy("APIUM GRAVEOLENS VAR. RAPACEUM 'GIANT PRAGUE'").cultivar).toBe('Giant Prague');
  expect(taxonomy('LILIUM BLANCHE NEIGE (‘BELOSNEZHKA’)').cultivar).toBe('Belosnezhka');
  expect(taxonomy("LILIUM BLANCHE NEIGE ('BELOSNEZHKA')").cultivar).toBe('Belosnezhka');
  expect(taxonomy("BEGONIA ( PENDULA GROUP ) 'CASCADE FLORENCE' ( CASCADE SERIES )").cultivar).toBe('Cascade Florence');
  expect(taxonomy("FORSYTHIA 'COURTASOL' MARÉE D'OR®").cultivar).toBe('Courtasol');
  expect(taxonomy("ERICA X DARLEYENSIS 'LUCIE' ( WINTER BELLES® SERIES )").cultivar).toBe('Lucie');
  expect(taxonomy("PHLOX DIVARICATA SUBSP. LAPHAMII 'CHATTAHOOCHEE'").cultivar).toBe('Chattahoochee');
  expect(taxonomy("ALLIUM PORRUM 'GROS LONG D'ÉTÉ 2'").cultivar).toBe("Gros Long d'Été 2");
  expect(taxonomy("FUCHSIA 'DELTA'S SARAH' BLUE SARAH").cultivar).toBe("Delta's Sarah");
});

test('Series', () => {
  expect(taxonomy('Hydrangea macrophylla var. hattoriana').series).toBeNull();
  expect(taxonomy("Begonia ( Pendula Group ) 'Cascade Florence' ( Cascade series )").series).toBe('Cascade');
  expect(taxonomy("Rosa 'Intereybabeuq' QUEEN BABYLON EYES® ( BABYLON EYES® series )").series).toBe('BABYLON EYES®');
  expect(taxonomy("Begonia (Groupe Darwinii) 'Cascade Florence' ( Cascade series )").series).toBe('Cascade');

  expect(taxonomy('HYDRANGEA MACROPHYLLA VAR. HATTORIANA').series).toBeNull();
  expect(taxonomy("BEGONIA ( PENDULA GROUP ) 'CASCADE FLORENCE' ( CASCADE SERIES )").series).toBe('CASCADE');
  expect(taxonomy("ROSA 'INTEREYBABEUQ' QUEEN BABYLON EYES® ( BABYLON EYES® SERIES )").series).toBe('BABYLON EYES®');
  expect(taxonomy("BEGONIA (GROUPE DARWINII) 'CASCADE FLORENCE' ( CASCADE SERIES )").series).toBe('CASCADE');
});

test('Group', () => {
  expect(taxonomy("Begonia ( Pendula Group ) 'Cascade Florence' ( Cascade series )").group).toBe('Pendula');
  expect(taxonomy('Abutilon Darwinii Group ').group).toBe('Darwinii');
  expect(taxonomy('Abutilon Groupe Darwinii').group).toBe('Darwinii');
  expect(taxonomy("Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").group).toBe('Darwinii');
  expect(taxonomy("Begonia (Groupe Darwinii) 'Cascade Florence' ( Cascade series )").group).toBe('Darwinii');
  expect(taxonomy('Abutilon (Darwinii Group) ').group).toBe('Darwinii');
  expect(taxonomy("Alcea rosea ( Chater's Double Group ) 'Yellow'").group).toBe('Chater\'s Double');
});

test('Commercial name', () => {
  expect(taxonomy("Begonia ( Pendula Group ) 'Cascade Florence' ( Cascade series )").commercialName).toBeNull();
  expect(taxonomy("Achillea 'Apricot Delight' ( TUTTI FRUTTI™ series )").commercialName).toBeNull();
  expect(taxonomy("Rosa 'Tanky' WHISKY").commercialName).toBe('WHISKY');
  expect(taxonomy('Lilium BLANCHE NEIGE (‘Belosnezhka’)').commercialName).toBe('BLANCHE NEIGE');
  expect(taxonomy("Forsythia 'Courtasol' MARÉE D'OR®").commercialName).toBe("MARÉE D'OR®");
  expect(taxonomy("Clematis 'Cleminov15' NANCY, JOUR DE LA TERRE® ( SAPHYRA® series )").commercialName).toBe('NANCY, JOUR DE LA TERRE®');

  expect(taxonomy("BEGONIA ( PENDULA GROUP ) 'CASCADE FLORENCE' ( CASCADE SERIES )").commercialName).toBeNull();
  expect(taxonomy("ACHILLEA 'APRICOT DELIGHT' ( TUTTI FRUTTI™ SERIES )").commercialName).toBeNull();
  expect(taxonomy("ROSA 'TANKY' WHISKY").commercialName).toBe('WHISKY');
  expect(taxonomy('LILIUM BLANCHE NEIGE (‘BELOSNEZHKA’)').commercialName).toBe('BLANCHE NEIGE');
  expect(taxonomy("FORSYTHIA 'COURTASOL' MARÉE D'OR®").commercialName).toBe("MARÉE D'OR®");
  expect(taxonomy("CLEMATIS 'CLEMINOV15' NANCY, JOUR DE LA TERRE® ( SAPHYRA® SERIES )").commercialName).toBe('NANCY, JOUR DE LA TERRE®');
});

test('Authors', () => {
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. macrophylla').author).toBe('(Thunb.) Spreng.');
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. macrophylla').subAuthor).toBeNull();
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. serrata (Thunb.) Makino.').author).toBe('(Thunb.) Spreng.');
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. serrata (Thunb.) Makino.').subAuthor).toBe('(Thunb.) Makino.');
});

test('Scientific name', () => {
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. macrophylla').getScientificName()).toBe('Hydrangea macrophylla (Thunb.) Spreng. subsp. macrophylla');
  expect(taxonomy("Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificName()).toBe("Begonia (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("xBegonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificName()).toBe("x Begonia (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("x Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificName()).toBe("x Begonia (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("+Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificName()).toBe("+ Begonia (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("+ Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificName()).toBe("+ Begonia (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy('Hydrangea x macrophylla (Thunb.) Spreng. subsp. macrophylla').getScientificName()).toBe('Hydrangea x macrophylla (Thunb.) Spreng. subsp. macrophylla');
  expect(taxonomy("Clematis 'Cleminov15' NANCY, JOUR DE LA TERRE® ( SAPHYRA® series )").getScientificName()).toBe("Clematis 'Cleminov15' NANCY, JOUR DE LA TERRE® (SAPHYRA® series)");
  expect(taxonomy("Cichorium endivia var. latifolium 'Blonde à Cœur Plein'").getScientificName()).toBe("Cichorium endivia var. latifolium 'Blonde à Cœur Plein'");
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. serrata (Thunb.) Makino.').getScientificName()).toBe('Hydrangea macrophylla (Thunb.) Spreng. subsp. serrata (Thunb.) Makino.');
});

test('Scientific name without author', () => {
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. macrophylla').getScientificNameWithoutAuthor()).toBe('Hydrangea macrophylla subsp. macrophylla');
  expect(taxonomy("Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificNameWithoutAuthor()).toBe("Begonia (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("xBegonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificNameWithoutAuthor()).toBe("x Begonia (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("x Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificNameWithoutAuthor()).toBe("x Begonia (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("+Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificNameWithoutAuthor()).toBe("+ Begonia (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("+ Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificNameWithoutAuthor()).toBe("+ Begonia (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy('Hydrangea x macrophylla (Thunb.) Spreng. subsp. macrophylla').getScientificNameWithoutAuthor()).toBe('Hydrangea x macrophylla subsp. macrophylla');
  expect(taxonomy("Clematis 'Cleminov15' NANCY, JOUR DE LA TERRE® ( SAPHYRA® series )").getScientificNameWithoutAuthor()).toBe("Clematis 'Cleminov15' NANCY, JOUR DE LA TERRE® (SAPHYRA® series)");
  expect(taxonomy("Cichorium endivia var. latifolium 'Blonde à Cœur Plein'").getScientificNameWithoutAuthor()).toBe("Cichorium endivia var. latifolium 'Blonde à Cœur Plein'");
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. serrata (Thunb.) Makino.').getScientificNameWithoutAuthor()).toBe('Hydrangea macrophylla subsp. serrata');
});

test('Scientific name HTML', () => {
  expect(taxonomy('Hydrangea macrophylla (Thunb.) Spreng. subsp. macrophylla').getScientificNameHtml()).toBe('<i>Hydrangea</i> <i>macrophylla</i> (Thunb.) Spreng. subsp. <i>macrophylla</i>');
  expect(taxonomy("Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificNameHtml()).toBe("<i>Begonia</i> (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("xBegonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificNameHtml()).toBe("x <i>Begonia</i> (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("x Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificNameHtml()).toBe("x <i>Begonia</i> (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("+Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificNameHtml()).toBe("+ <i>Begonia</i> (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy("+ Begonia ( Groupe Darwinii ) 'Cascade Florence' ( Cascade series )").getScientificNameHtml()).toBe("+ <i>Begonia</i> (Darwinii group) 'Cascade Florence' (Cascade series)");
  expect(taxonomy('Hydrangea x macrophylla (Thunb.) Spreng. subsp. macrophylla').getScientificNameHtml()).toBe('<i>Hydrangea</i> x <i>macrophylla</i> (Thunb.) Spreng. subsp. <i>macrophylla</i>');
  expect(taxonomy("Clematis 'Cleminov15' NANCY, JOUR DE LA TERRE® ( SAPHYRA® series )").getScientificNameHtml()).toBe("<i>Clematis</i> 'Cleminov15' NANCY, JOUR DE LA TERRE® (SAPHYRA® series)");
  expect(taxonomy("Cichorium endivia var. latifolium 'Blonde à Cœur Plein'").getScientificNameHtml()).toBe("<i>Cichorium</i> <i>endivia</i> var. <i>latifolium</i> 'Blonde à Cœur Plein'");
});
