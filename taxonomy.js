/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
// doc nomenclature :
// https://www.lescontessucculents.com/uploaded/96648/nomenclature-botanique-malecot.pdf
// http://arrosoirs-secateurs.com/Regles-et-termes-de-la
// SELECT TOP (1000) * FROM [PIM].[dbo].[article_plante]
const speciesList = require('./species');

const camelize = (str) => {
  const parts = str.toLowerCase().split(' ');
  let ret = '';

  for (let i = 0; i < parts.length; i += 1) {
    const word = parts[i].trim();
    if (word === 'à') {
      ret += ' à';
    } else if (word.length > 2 && word[1] === "'" && word[2] !== ' ') { // si apostrophe, c'est la 1ere lettre du mot apostrophé qui prend la majuscule
      ret += ` ${word[0]}'`;
      ret += `${word[2].toUpperCase()}`;
      if (word.length > 3) ret += word.slice(3);
    } else {
      ret += ` ${word[0].toUpperCase()}`;
      if (word.length > 1) ret += word.slice(1);
    }
  }
  return ret.trim();
};

const capitalize = (s) => s[0].toUpperCase() + s.slice(1).toLowerCase();

const findGenus = (nomLatin, taxonomy) => {
  const parts = nomLatin.split(' ');
  const word = parts[0];
  let taxonomyRaw = null;

  if ((word === 'x' || word === '×') && parts.length > 0) { // alt-158 pour le signe de muliplication ou la lettre x
    taxonomy.isHybridGenus = true;
    taxonomy.genus = `x ${capitalize(parts[1])}`;
    taxonomyRaw = `${word} ${parts[1]}`;
  } else if ((word[0] === '×' || word[0] === 'x') && word.length > 1) { // collé au mot, seul alt-158 est autorisé. la lettre x minuscule est normalement interdite mais on la tolère car elle est différenciable de la 1ere lettre du genre qui est en majuscule
    taxonomy.isHybridGenus = true;
    taxonomy.genus = `x ${capitalize(word.substring(1))}`;
    taxonomyRaw = word;
  } else if (word === '+' && parts.length > 0) {
    taxonomy.isChimaera = true;
    taxonomy.genus = `+ ${capitalize(parts[1])}`;
    taxonomyRaw = `${word} ${parts[1]}`;
  } else if (word[0] === '+') {
    taxonomy.isChimaera = true;
    taxonomy.genus = `+ ${capitalize(word.substring(1))}`;
    taxonomyRaw = word;
  } else {
    taxonomy.genus = capitalize(word);
    taxonomyRaw = word;
  }

  return taxonomyRaw;
};

const getCultivarRaw = (nomLatin) => {
  if (!nomLatin) return null;
  const regex = /['‘’].*?['‘’](?=$|\s|\))/g;
  const match = regex.exec(nomLatin);
  if (match) {
    return match[0].trim();
  }
  return null;
};

const getSeriesRaw = (nomLatin) => {
  if (!nomLatin) return null;

  const regex = /\([^(]+Series\s?\)/g;
  const match = regex.exec(nomLatin);

  if (match) {
    return match[0].trim();
  }
  return null;
};

const getGroupRaw = (text) => {
  if (!text) return null;
  if (text.includes('Groupe ')) {
    const regexFR = /\(?\s?Groupe\s?[A-Za-z\s']+\s?\)?/g;
    const matchFR = regexFR.exec(text);
    if (matchFR) {
      return matchFR[0].trim();
    }
  } else if (text.match(/ group(?=\)|\s|$)/gi)) {
    const regexEN = /\(?\s?[A-Za-z\s']+[Gg]roup?\s?\)?/g;
    const match = regexEN.exec(text);
    if (match && match[0].trim() !== '') {
      return match[0].trim();
    }
  }

  // nomLatin = nomLatin.replace(/ group\)/gi, ' Group)');
  // nomLatin = nomLatin.replace(/ group /gi, ' Group ');
  // nomLatin = nomLatin.replace(/ group$/gi, ' Group');

  return null;
};

const getCommercialNameRaw = (text) => {
  if (!text) return null;
  let ret = null;

  while (!ret) {
    if (text.startsWith('var.')
        || text.startsWith('var ')
        || text.startsWith('subsp.')
        || text.startsWith('subsp ')) {
      const parts = text.split(' ');
      text = '';
      for (let i = 2; i < parts.length; i += 1) text += parts[i];
    }
    const regex = /[A-Z'@][^a-zàèìòùáéíóúýâêîôûãñõäëïöüÿçøåæœ()]+/g;
    const match = regex.exec(text);
    if (match) {
      ret = match[0].trim();
      const retLower = ret.toLocaleLowerCase();
      if (speciesList.includes(retLower)) { // cas où l'espece est à tort en majuscule
        text = text.replace(ret, ' ').replace(/\s\s/g, ' ').replace(/\s\s/g, ' ').trim();
        ret = null;
      }
    } else {
      break;
    }
  }
  return ret;
};

const getTaxonomy = (nomLatin) => {
  if (!nomLatin) return null;

  nomLatin = nomLatin.trim().replace(/\s\s/, ' ');
  nomLatin = nomLatin.replace(/SUBSP/, 'subsp');
  nomLatin = nomLatin.replace(/\sVAR\./, ' var.');
  nomLatin = nomLatin.replace(/\sVAR\s/, ' var ');
  nomLatin = nomLatin.replace(/groupe /gi, 'Groupe ');
  nomLatin = nomLatin.replace(/ group\)/gi, ' Group)');
  nomLatin = nomLatin.replace(/ group /gi, ' Group ');
  nomLatin = nomLatin.replace(/ group$/gi, ' Group');
  nomLatin = nomLatin.replace(/ series? /gi, ' Series ');
  nomLatin = nomLatin.replace(/\?/gi, '');
  nomLatin = nomLatin.replace(/\s{3}/gi, ' ');
  nomLatin = nomLatin.replace(/\s{2}/gi, ' ');
  if (nomLatin.trim() === '') return null;

  const taxonomy = {
    genus: null,
    species: null,
    subspecies: null,
    variety: null,
    cultivar: null,
    commercialName: null,
    author: null,
    subAuthor: null,
    series: null,
    group: null,
    isHybridGenus: false,
    isHybridSpecies: false,
    isChimaera: false,

    getScientificName() {
      let text = this.genus;
      if (this.group) text += ` (${this.group} group)`;
      if (this.species) text += ` ${this.species}`;
      if (this.author) text += ` ${this.author}`;
      if (this.subspecies) text += ` subsp. ${this.subspecies}`;
      if (this.variety) text += ` var. ${this.variety}`;
      if (this.subAuthor) text += ` ${this.subAuthor}`;
      if (this.cultivar) text += ` '${this.cultivar}'`;
      if (this.commercialName) text += ` ${this.commercialName}`;
      if (this.series) text += ` (${this.series} series)`;
      return text;
    },

    getScientificNameWithoutAuthor() {
      let text = this.genus;
      if (this.group) text += ` (${this.group} group)`;
      if (this.species) text += ` ${this.species}`;
      if (this.subspecies) text += ` subsp. ${this.subspecies}`;
      if (this.variety) text += ` var. ${this.variety}`;
      if (this.cultivar) text += ` '${this.cultivar}'`;
      if (this.commercialName) text += ` ${this.commercialName}`;
      if (this.series) text += ` (${this.series} series)`;
      return text;
    },

    getScientificNameHtml() {
      let text = '';
      if (this.isHybridGenus) {
        text += `x <i>${this.genus.replace('x ', '')}</i>`;
      } else if (this.isChimaera) {
        text += `+ <i>${this.genus.replace('+ ', '')}</i>`;
      } else {
        text += `<i>${this.genus}</i>`;
      }

      if (this.group) text += ` (${this.group} group)`;

      if (this.species) {
        if (this.isHybridSpecies) {
          text += ` x <i>${this.species.replace('x ', '')}</i>`;
        } else {
          text += ` <i>${this.species}</i>`;
        }
      }

      if (this.author) text += ` ${this.author}`;
      if (this.subspecies) text += ` subsp. <i>${this.subspecies}</i>`;
      if (this.variety) text += ` var. <i>${this.variety}</i>`;
      if (this.subAuthor) text += ` ${this.subAuthor}`;
      if (this.cultivar) text += ` '${this.cultivar}'`;
      if (this.commercialName) text += ` ${this.commercialName}`;
      if (this.series) text += ` (${this.series} series)`;
      return text;
    },
  };

  // genus
  const taxonomyRaw = findGenus(nomLatin, taxonomy);
  if (!taxonomyRaw) return null;

  nomLatin = nomLatin.replace(taxonomyRaw, '').trim();

  // détermine et enlève les elements qui peuvent se placer dans n'importe quel ordre
  // series et group avant commercialName et cultivar pour supprimer les syntaxes apparentes mais qui ne sont pas des commercialName et cultivar
  // puis commercialName avant cultivar pour extraire les caractères spéciaux présents dans les textes en majuscules (quotes...)
  // series
  const seriesRaw = getSeriesRaw(nomLatin);
  if (seriesRaw) {
    nomLatin = nomLatin.replace(seriesRaw, ' ').replace(/\s\s/g, ' ').replace(/\s\s/g, ' ').trim();
    taxonomy.series = seriesRaw.replace(/[()]/g, ' ').replace(/serie[s]?/gi, '').trim();
  }
  // group
  const groupRaw = getGroupRaw(nomLatin);
  if (groupRaw) {
    nomLatin = nomLatin.replace(groupRaw, ' ').replace(/\s\s/g, ' ').replace(/\s\s/g, ' ').trim();
    taxonomy.group = groupRaw.replace(/[()]/g, ' ').replace(/group[e]?/gi, '').trim();
  }

  // cultivar
  const cultivarRaw = getCultivarRaw(nomLatin);
  if (cultivarRaw) {
    nomLatin = nomLatin.replace(cultivarRaw, ' ').replace(/\(\)/g, '').replace(/\s\s/g, ' ').replace(/\s\s/g, ' ')
      .trim();
    taxonomy.cultivar = camelize(cultivarRaw.substring(1, cultivarRaw.length - 1));
  }

  // commercialName
  const commercialNameRaw = getCommercialNameRaw(nomLatin);
  if (commercialNameRaw) {
    nomLatin = nomLatin.replace(commercialNameRaw, ' ').replace(/\s\s/g, ' ').replace(/\s\s/g, ' ').trim();
    taxonomy.commercialName = commercialNameRaw.trim();
  }

  const parts = nomLatin.split(' ');

  for (let i = 0; i < parts.length; i += 1) {
    const word = parts[i];
    const wordLower = word.toLowerCase();

    // subspecies
    if (wordLower === 'subsp' || wordLower === 'subsp.') {
      if (i + 1 < parts.length) {
        taxonomy.subspecies = parts[i + 1].toLowerCase();
        i += 1;
      }
    } else if (wordLower === 'var' || wordLower === 'var.') { // variety
      if (i + 1 < parts.length) {
        taxonomy.variety = parts[i + 1].toLowerCase();
        i += 1;
      }
    } else if (taxonomy.species) { // author
      if (taxonomy.subspecies || taxonomy.variety) {
        taxonomy.subAuthor = taxonomy.subAuthor ? `${taxonomy.subAuthor} ${word}` : word;
      } else {
        taxonomy.author = taxonomy.author ? `${taxonomy.author} ${word}` : word;
      }
    }

    // species
    if (!taxonomy.species) {
      if ((word === 'x' || word === '×') && i + 1 < parts.length) { // alt-158 pour le signe de multiplication ou la lettre x
        taxonomy.isHybridSpecies = true;
        taxonomy.species = `x ${parts[i + 1].toLowerCase()}`;
        i += 1;
      } else if (word[0] === '×' && word.length > 1) { // collé au mot, la lettre x est interdite, seul alt-158 est autorisé
        taxonomy.isHybridSpecies = true;
        taxonomy.species = `x ${word.substring(1).toLowerCase()}`;
      } else if (word.match(/[^A-Z]+/)) {
        taxonomy.species = word.toLowerCase();
      } else if (speciesList.includes(word.toLowerCase())) {
        taxonomy.species = word.toLowerCase();
      }
    }
  }

  return taxonomy;
};

module.exports = getTaxonomy;

// console.log(getTaxonomy("Alcea rosea ( Chater's Double Group ) 'Yellow'"));
